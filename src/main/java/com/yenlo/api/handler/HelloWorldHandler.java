package com.yenlo.api.handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.synapse.ManagedLifecycle;
import org.apache.synapse.MessageContext;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.rest.AbstractHandler;

/**
 *
 * @author reverts
 */
public class HelloWorldHandler extends AbstractHandler implements ManagedLifecycle {
    
    private static final Log LOG = LogFactory.getLog(HelloWorldHandler.class);
    
    @Override
    public boolean handleRequest(MessageContext mc) {
        LOG.info("Request: Hello world");
        
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext mc) {
        LOG.info("Response: Hello world");
        
        return true;
    }

    @Override
    public void init(SynapseEnvironment se) {
        LOG.info("Initialize: Hello world");
    }

    @Override
    public void destroy() {
        LOG.info("Destroy: Hello world");
    }
    
}
